// ==UserScript==
// @name         Dark Wayfarer
// @version      0.1
// @description  Dark theme for Wayfarer
// @match        https://wayfarer.nianticlabs.com/review
// @downloadURL  https://gitlab.com/AlfonsoML/darkwayfarer/raw/master/darkwayfarer.user.js
// @supportURL   https://gitlab.com/AlfonsoML/darkwayfarer/issues

// ==/UserScript==

/*
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

(function() {
	'use strict';

	function addCss() {

	const css = `
body{
    background:black !important;
     color: grey !important;
}
 h1,h2,h3,h4,h5,h6,h7,h8,h9{
    color:white !important;
}
 #gallery-info{
    background: #0F0F0F !important;
}
 .niantic-wayfarer-logo img{
    filter: invert(1) !important;
}
 .header{
    background: black !important;
}
 .card{
    background: black !important;
}
 .card-header__title{
    color: white !important;
}
 .supporting-central-field{
    background: black !important;
}
 .title-description{
    color: white !important;
}
 #WhatIsItController .categories-display .categories-display-container ul li .categories-display-name{
    color: white !important;
}
 textarea{
    color: white !important;
}
 .modal-content, .modal-body{
    background:black !important;
    color:white !important;
}
 .supporting-statement-central-field{
    background: black !important;
     color: white !important;
}
 .known-information-need-edit {
    background: black !important;
}
 .modal-body {
    background: black;
     box-shadow: #1F1F1F 2px 2px 10px !important;
}
 input, text-input {
    color: white !important;
}
 .button-primary {
    background: #1B8DAD !important;
}
 .button-primary:focus,.button-primary:hover {
    background: #28A8CB !important;
}
 .nomination-title, .nomination-category-body {
    color: white !important;
}
 #nom-search-title {
    background: black !important;
     color: white !important;
}
 .supporting-statement-central-field p, .item-text, .supporting-statement-central-field {
    color: white !important;
}
 #nom-options-button {
    filter: invert(1) !important;
}
 .gm-style .gm-style-iw-c, .gm-style .gm-style-iw-d, .duplicate-map-popup-title {
    color:black !important;
}
 .showcase-container, .niantic-loader {
    background: black !important;
}
 .niantic-loader__logo {
    filter: none !important;
}
 .switch-label {
    filter: invert();
}
 .sidebar .sidebar-item.--selected, .sidebar .sidebar-item:hover {
    background: #1F1F1F !important;
     border-left: #20B8E3 5px solid !important;
}
 .sidebar {
    background: #0C0C0C !important;
}
 .star-red-orange, .selected>.star-gray {
    color: #20B8E3 !important;
}
 @keyframes shadow2 {
     from,to {
        background: rgba(255,255,255,.4);
         filter: blur(4px);
    }
     55% {
        background: rgba(255,255,255,.2);
         filter: blur(6px);
    }
}
 .niantic-loader__shadow {
    animation: shadow2 2.2s ease-in-out infinite !important;
}
 .nomination-status--next-upgrade, .nomination-status--upgrade {
    color: #6200EE !important;
}
 p {
    color: #C0C0C0 !important;
}
 #WhatIsItController .categories-display .categories-display-result::before {
    filter: invert() !important;
}
 #WhatIsItController .categories-display .categories-display-result {
    color: white !important;
}
 #portalReviewTimer {
    display: inline-block;
}
 .reset-map-icon {
    filter: invert();
}

`;
		const style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = css;
		document.head.appendChild(style);
	}

	addCss();

})();